# InputMap

Un campo de texto que permite agregar y quitar valores de una lista
predefinida solo escribiendo sus etiquetas.

Es un reemplazo liviano para select2 y librerías similares.  Pero no
reimplementa los campos en `<div>`s dándoles estilo, sino que reutiliza
los tipos de campos de formulario ya establecidos en el estándar HTML.

De esta forma no hay que replicar comportamiento y se aprovechan todas
las características de accesibilidad disponibles en los navegadores.

No incluye ningún tipo de estilo para que podamos darle el estilo que
queramos.

Está desarrollado con [Svelte](https://svelte.dev/)

## Utilizar

Leer el archivo `index.html` :)

## Desarrollo

Modificar el archivo `src/input-map.svelte`

Compilar con `npm run build`.  Esto genera los archivos `input-map.js` e
`input-map.mjs` para utilizar de forma independiente.

## Propiedades

Las `prop(iedade)s` que acepta son:

* `defaultValues` los valores pre-cargados, un objeto JSON de `{"etiqueta": "valor"}`

* `values` los valores actuales, un array JSON `["valor","valor2"]`

* `name` es el nombre del campo que se envía junto con el
  formulario.  Vamos a querer que termine en `[]` para que se envíen
  muchos valores juntos.

* `list` es el atributo `id` de una etiqueta `datalist`

  <https://developer.mozilla.org/es/docs/Web/HTML/Elemento/datalist>

* `remove` es una opción binaria para deshabilitar la eliminación del
  campo si lo desactivamos.

* `legend` la descripción del grupo de valores

* `described` el atributo `id` de un elemento que describe a este.  Es
  igual a utilizar el atributo `aria-describedby`.

* `button` el nombre del botón

## Estilos

¡No se provee ningún estilo por defecto!  Hay que tener cuidado al dar
estilo a los campos con `checkbox`, porque si lo modificamos para que no
muestre el recuadro para tildar (que se vea como una píldora como en
select2, por ejemplo), no podemos hacerle foco con el teclado ni
habilitarlo y deshabilitarlo con la barra espaciadora.
